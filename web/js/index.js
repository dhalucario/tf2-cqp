// import 'bootstrap/js/src/collapse';
import TomSelect from 'tom-select';

const tags = [
    '1vs1',
    'academy',
    'achievement',
    'allcrits',
    'alltalk',
    'arena',
    'bhop',
    'cp',
    'ctf',
    'custom',
    'deathmatch',
    'dm',
    'fastdl',
    'friendlyfire',
    'gravity',
    'idle',
    'increased_maxplayers',
    'jail',
    'koth',
    'medival',
    'mge',
    'mvm',
    'nocrits',
    'nodmgspread',
    'norespawntime',
    'payload',
    'plr',
    'rd',
    'respawntimes',
    'rtd',
    'rtv',
    'surf',
    'trade',
    'trading',
    'x10',
    'x100'
];

document.addEventListener('DOMContentLoaded', () => {
    let tomSelects = [];
    let tomConfig = {
        create: true,
        createOnBlur: true,
        maxItems: null,
        options: tags.map((tag) => { return { value: tag, text: tag } }),
    };

    tomSelects.push(new TomSelect('#tags-filter', tomConfig));
    tomSelects.push(new TomSelect('#tags-blacklist', tomConfig));

    function invoke(msg) {
        if (window.external) {
            window.external.invoke(msg);
        } else {
            window.webkit.messageHandlers.external.postMessage(msg);
        }
    }

    let findMatchButton = document.getElementById('find-match');
    if (findMatchButton === null) return;

    findMatchButton.addEventListener('click', (e) => {
        e.preventDefault();
        let filterCollapse = document.getElementById('filter-collapse');
        let fields = filterCollapse.querySelectorAll('input.filter, select.filter');

        let settings = {
            keybool: {},
            keyval: {}
        };

        for (let i = 0; i < fields.length; i++) {
            switch (fields[i].type) {
                case 'checkbox':
                    settings.keybool[fields[i].id] = fields[i].checked;
                    break;
                default:
                    settings.keyval[fields[i].id] = fields[i].value;
                    break;
            }

        }

        for (let i = 0; i < tomSelects.length; i++) {
            settings[tomSelects[i].inputId] = tomSelects[i].getValue();
        }

        invoke(JSON.stringify({
            action: 'connect',
            data: JSON.stringify(settings)
        }));
    });
});
