# TF2 Community Quickplay

Since Valve decided to remove the casual quickplay functionality from TF2
I decided to reimplement it myself in Rust with an external program.
So far this program has only been tested on Windows but it should run on Linux without any problems.

## Usage

You run the application and click the "Find match!" button.

## Setup

You will need following things:  
[rustup](https://rustup.rs/) (I used Rust nightly but it should also work with stable)  
[NodeJS](https://nodejs.org/)  
[Parcel 2](https://parceljs.org/)  

## Linux Dependencies

Fedora 34:

``` bash
sudo dnf install -y libsoup-devel gtk3-devel webkit2gtk3-devel
```

## Compilation

``` bash
git clone git@gitlab.com:dhalucario/tf2-cqp.git
cd tf2-cqp
npm install
npm run build

cargo build --release
```

Then you can use the executable you find in `target/release/casual-quickplay.exe`

## TODO's
* Minimize Bootstrap CSS usage
