use a2s::A2SClient;
use msq::{MSQClient, Filter, Region};
use rand::prelude::*;
use serde::{Deserialize};
use std::sync::Arc;
use tokio::sync::Mutex;
use web_view::*;
use std::collections::HashMap;
use a2s::info::Info;

pub struct CommunityQuickplayController {
    msq_client: Arc<Mutex<MSQClient>>,
    a2s_client: Arc<Mutex<A2SClient>>,
}

#[derive(Debug)]
struct GameServer {
    ip: String,
    ping: u128,
    server_info: Info
}

#[derive(Debug, Deserialize)]
pub struct Action {
    action: String,
    data: String,
}

#[derive(Debug, Deserialize)]
pub struct ServerFilter {
    keybool: HashMap<String, bool>,
    keyval: HashMap<String, String>,

    #[serde(alias = "tags-filter")]
    filter: Vec<String>,

    #[serde(alias = "tags-blacklist")]
    blacklist: Vec<String>
}

impl CommunityQuickplayController {
    pub async fn new() -> CommunityQuickplayController {
        let msq_client = Arc::new(Mutex::new(MSQClient::new().await.expect("Unable to set up MSQClient")));
        let a2s_client = Arc::new(Mutex::new(A2SClient::new().await.expect("Unable to set up A2SClient")));

        CommunityQuickplayController {
            msq_client,
            a2s_client
        }
    }

    pub async fn run(self) {
        let content = include_str!("../dist/index.html");

        let msq_client = self.msq_client.clone();
        let a2s_client = self.a2s_client.clone();

        web_view::builder()
            .debug(false)
            .title("Community Quickplay")
            .content(Content::Html(content))
            .size(500, 650)
            .resizable(false)
            .user_data(())
            .invoke_handler(move |webview, arg| {
                let arc_mutex_msq_client = msq_client.clone();
                let arc_mutex_a2s_client = a2s_client.clone();
                let webview_handle = webview.handle();
                let action: Action = serde_json::from_str(arg).expect("Could not decode response.");

                tokio::task::spawn(async move {
                    if let Err(err) = CommunityQuickplayController::invoke_handler(webview_handle, arc_mutex_msq_client, arc_mutex_a2s_client, action).await {
                        println!("{:?}", err);
                    }
                });

                Ok(())
            })
            .run()
            .expect("Initalizing webview failed");
    }

    pub fn str_to_region(input: &str) -> Region {
        match input {
            "USEast" => {
                Region::USEast
            }
            "USWest" => {
                Region::USWest
            }
            "SouthAmerica" => {
                Region::SouthAmerica
            }
            "Europe" => {
                Region::Europe
            }
            "Asia" => {
                Region::Asia
            }
            "MiddleEast" => {
                Region::MiddleEast
            }
            "Africa" => {
                Region::Africa
            }
            _ => {
                Region::All
            }
        }
    }

    pub async fn invoke_handler(
        webview_handle: web_view::Handle<()>,
        msq_client: Arc<Mutex<MSQClient>>,
        a2s_client: Arc<Mutex<A2SClient>>,
        action: Action
    ) -> WVResult {
        let mut msq_client = msq_client.lock().await;
        let action_data: ServerFilter = serde_json::from_str(action.data.as_str()).expect("Action data is invalid");

        // TODO: Query server info. Collect ping time.
        let a2s_client = a2s_client.lock().await;

        msq_client.connect("hl2master.steampowered.com:27011").await.expect("Could not connect to the master server.");
        msq_client.max_servers_on_query(usize::MAX);

        let filter = action_data.filter.iter().map(|s| s as &str).collect();
        let blacklist: Vec<&str> = action_data.blacklist.iter().map(|s| s as &str).collect();
        let region = CommunityQuickplayController::str_to_region(action_data.keyval.get("server-region").unwrap().as_str());

        // let secure = !*action_data.keybool.get("server-insecure").expect("Could not find secure value.");
        let empty = *action_data.keybool.get("server-empty").expect("Could not find empty value.");

        let mut msq_ips = msq_client
            .query(
                region,
                Filter::new()
                    .appid(440)
                    .secure(true)
                    .password(true)
                    .full(false)
                    .empty(empty)
                    .gametype(&filter)
            )
            .await.expect("Error while getting servers");

        msq_ips.shuffle(&mut rand::thread_rng());

        println!("Fetching servers...");

        let mut game_server: Option<GameServer> = None;
        'server: for ip in msq_ips {
            println!("Checking server {}", ip);
            let time = std::time::Instant::now();
            let a2s_res = a2s_client.info(ip.clone()).await;
            let ping = time.elapsed().as_millis();

            if let Ok(server_info) = a2s_res {
                if server_info.visibility == true {
                    println!("Server does not satisfy our needs (password)");
                    continue;
                }

                if let Some(keywords) = server_info.extended_server_info.keywords.clone() {
                    for tag in &blacklist {
                        if keywords.contains(tag) {
                            println!("Server does not satisfy our needs (blacklist)");
                            continue 'server;
                        }
                    }

                    for tag in &filter {
                        if !keywords.contains(tag) {
                            println!("Server does not satisfy our needs (filter)");
                            continue 'server;
                        }
                    }
                }

                game_server = Some(GameServer {
                    ip,
                    ping,
                    server_info
                });
            } else {
                println!("Server does not respond");
            }

            if game_server.is_some() {break;}
        }

        /*if !secure {
            game_servers = game_servers.into_iter().filter(|server| {
                server.server_info.vac == false
            }).collect();
        }*/

        if let Some(game_server) = game_server {
            println!("Connecting to {}", game_server.ip);
            if let Err(_err) = webbrowser::open(format!("steam://connect/{}", game_server.ip).as_str()) {
                webview_handle.dispatch(|webview| {
                    webview.eval("alert('I was not able to open the link');").expect("Could not report error");
                    Ok(())
                }).expect("Could not dispatch using handle");
            }
        } else {
            webview_handle.dispatch(|webview| {
                webview.eval("alert('Could not find any servers');").expect("Could not report error");
                Ok(())
            }).expect("Could not dispatch using handle");
        }

        Ok(())
    }
}
