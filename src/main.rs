mod community_quickplay_controller;

use crate::community_quickplay_controller::CommunityQuickplayController;
use std::io::Result;

#[tokio::main]
async fn main() -> Result<()> {
    let cqc = CommunityQuickplayController::new().await;
    cqc.run().await;
    Ok(())
}
